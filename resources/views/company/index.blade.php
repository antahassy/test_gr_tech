@extends('layouts.app')

@section('content')
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    table tr td{
        vertical-align: middle !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $active_menu }}</div>

                <div class="card-body">
                    <div style="margin-bottom: 15px;">
                        <button class="btn btn-primary font-weight-bolder btn-sm" id="btn_add">Add Data</button>
                    </div>
                    <div>
                        <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                            <thead class="thead-dark">
                                <tr> 
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>Website</th>
                                    <th>Action</th> 
                                    <th>Created</th> 
                                    <th>Updated</th> 
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" id="id_data" name="id_data">
                    <input type="hidden" name="_method" id="_method">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Company Name</label>
                            <input type="text" name="company_name" id="company_name" class="form-control">
                            <label>Company Email</label>
                            <input type="text" name="company_email" id="company_email" class="form-control">
                            <label>Company Website</label>
                            <input type="text" name="company_website" id="company_website" class="form-control">

                            <label>Company Logo</label>
                            <input type="hidden" name="delete_file" id="delete_file" value="">
                            <input type="hidden" name="get_file" id="get_file" value="">
                            <input type="file" name="file" id="file" accept="image/*" style="width: 100%;">
                            <div>
                                <div id="delete_preview_items">Delete Logo</div>
                                <img id="preview_items" src="" title="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers     : {
                'X-CSRF-TOKEN'  : $('meta[name="csrf-token"]').attr('content')
            }
        });
        var nama_bulan = new Array();
        nama_bulan[1] = 'January';
        nama_bulan[2] = 'February';
        nama_bulan[3] = 'March';
        nama_bulan[4] = 'April';
        nama_bulan[5] = 'May';
        nama_bulan[6] = 'June';
        nama_bulan[7] = 'July';
        nama_bulan[8] = 'August'; 
        nama_bulan[9] = 'September'; 
        nama_bulan[10] = 'October';
        nama_bulan[11] = 'November'; 
        nama_bulan[12] = 'December';
        var table_data = '';
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    data_table();
                },500);
            }
        });
        function main(){
            var modal_form;
            $('#modal_form').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_form = true;
            });
            $('#modal_form').on('hide.bs.modal', function(){
                if(modal_form){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_form = false;
                    setTimeout(function(){
                        $('#modal_form').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
            $('#btn_add').on('click', function(){
                $('#form_data')[0].reset();
                $('#modal_form').find('.modal-title').text('Add Data');
                $('#btn_process').text('Save');
                $('#form_data').attr('method', 'post');
                $('#_method').val('post');
                $('#form_data').attr('action', site + '/company');
                $('#file, #get_file, #delete_file').val('');
                $('#modal_form').modal('show');
                display_image();
                data_process();
            });
            $('#table_data').on('click', '.btn_edit', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'get',
                                url            : site + '/company/' + action_data.id + '/edit',
                                async          : true,
                                dataType       : 'json',
                                success        : function(data){
                                    $('#form_data')[0].reset();
                                    $('#company_name').val(data.name);
                                    $('#company_email').val(data.email);
                                    
                                    if(! data.logo){
                                        $('#get_file, #delete_file').val('');
                                        $('#preview_items').attr('src', site + '/logo/company_logo.png');
                                        $('#delete_preview_items').hide();
                                    }else{
                                        $('#get_file, #delete_file').val(data.logo);
                                        $('#preview_items').attr('src', site + '/' + data.logo);
                                        $('#delete_preview_items').show();
                                    }
                                    $('#file').val('');
                                    display_image(); 

                                    $('#company_website').val(data.website);
                                    $('#modal_form').find('.modal-title').text('Edit Data');
                                    $('#btn_process').text('Update');
                                    $('#form_data').attr('method', 'post');
                                    $('#_method').val('put');
                                    $('#form_data').attr('action', site + '/company/' + data.id);
                                    swal.close();
                                    $('#modal_form').modal('show');
                                    data_process();
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
            });
            $('#table_data').on('click', '.btn_delete', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    html                : '<pre>Company information' + '<br>' + 
                                          'On employees will be lost' + '<br>' + 
                                          'Delete this data ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : action_data.id},
                                        url         : site + '/company/delete',
                                        dataType    : "json",
                                        async       : true,
                                        success   : function(response){
                                            if(response.success){
                                                swal({
                                                    html                : '<pre>Data deleted successfully</pre>',
                                                    type                : "success",
                                                    background          : 'transparent',
                                                    allowOutsideClick   : false,
                                                    allowEscapeKey      : false, 
                                                    showConfirmButton   : false,
                                                    timer               : 1000
                                                }).then(function(){
                                                    setTimeout(function(){
                                                        table_data.ajax.reload();
                                                    },500);
                                                });
                                            }
                                        },
                                        error          : function(){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
        }
        function data_process(){
            $('#btn_process').on('click', function(event){
                event.preventDefault();
                event.stopImmediatePropagation();
                var ajax_method     = $('#form_data').attr('method');
                var ajax_url        = $('#form_data').attr('action');
                var form_data       = $('#form_data')[0];
                form_data           = new FormData(form_data);
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : ajax_method,
                                url            : ajax_url,
                                data           : form_data,
                                async          : true,
                                processData    : false,
                                contentType    : false,
                                cache          : false,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.error){
                                        var errormessage    = '';
                                        for(i = 0; i < response.message.length; i ++){
                                            errormessage += response.message[i] + '\n';
                                        }
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>' + errormessage + '</pre>'
                                        });
                                    }
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        $('#form_data')[0].reset();
                                        swal({
                                            html                : '<pre>Data ' + response.type + ' successfully</pre>',
                                            type                : "success",
                                            background          : 'transparent',
                                            allowOutsideClick   : false,
                                            allowEscapeKey      : false, 
                                            showConfirmButton   : false,
                                            timer               : 1000
                                        }).then(function(){
                                            setTimeout(function(){
                                                table_data.ajax.reload();
                                            },500);
                                        });
                                    }
                                    if(response.size){
                                        swal({
                                            html                : '<pre>File size too big' + '<br>' + 
                                                                  'Max 5 mb</pre>',
                                            type                : "warning",
                                            background          : 'transparent'
                                        });
                                    }
                                    if(response.extension){
                                        swal({
                                            html                : '<pre>Invalid file extension</pre>',
                                            type                : "warning",
                                            background          : 'transparent'
                                        });
                                    }
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
                return false;
            });
        }
        function display_image(){
            function preview(image){
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
            $("#file").on('change', function(){
                preview(this);
                var names = $(this).val();
                var file_names = names.replace(/^.*\\/, "");
            });
            $('#delete_preview_items').on('click', function(){
                $('#delete_preview_items').css('display','none');
                $('#preview_items').attr('src', '');
                $('#file, #get_file').val('');
            });
        }
        function data_table(){
            table_data = $('#table_data').DataTable({
                lengthMenu          : [ [5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"] ],
                processing          : true, 
                destroy             : true,
                serverSide          : true, 
                scrollX             : true,
                scrollCollapse      : true,
                fixedColumns        : true, 
                initComplete: function(){
                    swal.close();
                    main();
                },
                ajax            : {
                    url         : site + '/company',
                    method      : 'get'
                },
                columns             : [
                    {data   : 'DT_RowIndex'},
                    {data   : 'name'},
                    {data   : 'email'},
                    {data   : 'logo',
                        render: function(logo){
                            if(logo != null && logo != ''){
                                return '<a href="' + site + '/' + logo + '" target="_blank"><img src="' + site + '/' + logo + '" style="width: 75px; height: 75px; border-radius: 100%;"></a>';
                            }else{
                                return '';
                            }
                        }
                    },
                    {data   : 'website',
                        render: function(website){
                            return '<a href="http://' + website + '" target="_blank">http://' + website + '</a>'; 
                        }
                    },
                    {data   : 'action'},
                    {data   : 'created_at',
                        render: function(created_at){
                            var time = created_at.split(' ');
                            return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                        }
                    },
                    {data   : 'updated_at',
                        render: function(updated_at){
                            if(updated_at != null){
                                var time = updated_at.split(' ');
                                return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                            }else{
                                return '';
                            }
                        }
                    }
                ]
            });
        }
    })
</script>
@endsection