@extends('layouts.app')

@section('content')
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    table tr td{
        vertical-align: middle !important;
    }
    #table_company tr td{
        text-align: left !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $active_menu }}</div>

                <div class="card-body">
                    <div style="margin-bottom: 15px;">
                        <button class="btn btn-primary font-weight-bolder btn-sm" id="btn_add">Add Data</button>
                    </div>
                    <div>
                        <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                            <thead class="thead-dark">
                                <tr> 
                                    <th>No</th>
                                    <th>Full Name</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th> 
                                    <th>Created</th> 
                                    <th>Updated</th> 
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" id="id_data" name="id_data">
                    <input type="hidden" name="_method" id="_method">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>First Name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control">
                            <label>Last Name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control">
                            <label>Company</label>
                            <select class="form-control" name="company" id="company">
                                <option value="">Select Company</option>
                            </select>
                            <label>Email</label>
                            <input type="text" name="email" id="email" class="form-control">
                            <label>Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_company" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Company</h5>
            </div>
            <div class="modal-body"> 
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center">
                        <div id="company_logo"></div>
                        <table id="table_company" style="width: 100%;">
                            <tr>
                                <td>Company Name</td>
                                <td id="company_name"></td>
                            </tr>
                            <tr>
                                <td>Company Website</td>
                                <td id="company_website"></td>
                            </tr>
                            <tr>
                                <td>Company Email</td>
                                <td id="company_email"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers     : {
                'X-CSRF-TOKEN'  : $('meta[name="csrf-token"]').attr('content')
            }
        });
        var nama_bulan = new Array();
        nama_bulan[1] = 'January';
        nama_bulan[2] = 'February';
        nama_bulan[3] = 'March';
        nama_bulan[4] = 'April';
        nama_bulan[5] = 'May';
        nama_bulan[6] = 'June';
        nama_bulan[7] = 'July';
        nama_bulan[8] = 'August'; 
        nama_bulan[9] = 'September'; 
        nama_bulan[10] = 'October';
        nama_bulan[11] = 'November'; 
        nama_bulan[12] = 'December';
        var table_data = '';
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    data_table();
                },500);
            }
        });
        function s_company(company_id){
            $.ajax({
                type        : 'ajax',
                method      : 'post',
                url         : site + '/employee/s_company',
                dataType    : "json",
                async       : false,
                success: function(company_data){
                    if(company_data.length == 0){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Company list not available</pre>',
                            type        : "warning"
                        }).then(function(){
                            $('#modal_form').modal('show');
                            data_process();
                        });
                    }else{
                        var company = '<option value="">Select Company</option>';
                        for(i = 0; i < company_data.length; i ++){
                            company += '<option value="' + company_data[i].id + '">' + company_data[i].name + '</option>';
                        }
                        $('#company').html(company);
                        if(company_id != ''){
                            $('#company').val(company_id);
                        }else{
                            $('#company').val('');
                        }
                        swal.close();
                        $('#modal_form').modal('show');
                        data_process();
                    }
                },
                error: function (){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Connection lost' + '<br>' + 
                                      'Please try again</pre>',
                        type        : "warning"
                    });
                }
            });
        }
        function main(){
            var modal_form;
            $('#modal_form').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_form = true;
            });
            $('#modal_form').on('hide.bs.modal', function(){
                if(modal_form){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_form = false;
                    setTimeout(function(){
                        $('#modal_form').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
            var modal_company;
            $('#modal_company').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_company = true;
            });
            $('#modal_company').on('hide.bs.modal', function(){
                if(modal_company){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_company = false;
                    setTimeout(function(){
                        $('#modal_company').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
            $('#btn_add').on('click', function(){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $('#form_data')[0].reset();
                            $('#modal_form').find('.modal-title').text('Add Data');
                            $('#btn_process').text('Save');
                            $('#form_data').attr('method', 'post');
                            $('#_method').val('post');
                            $('#form_data').attr('action', site + '/employee');
                            var company_id = '';
                            s_company(company_id);
                        },500);
                    }
                });
            });
            $('#table_data').on('click', '.btn_company_detail', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                var logo_company = '<a href="' + site + '/' + action_data.company_logo + '" target="_blank"><img src="' + site + '/' + action_data.company_logo + '" style="width: 150px; height: 150px; border-radius: 100%;"></a>';
                var website_company = '<a href="http://' + action_data.company_website + '" target="_blank">http://' + action_data.company_website + '</a>';
                $('#company_logo').html(logo_company); 
                $('#company_name').html(action_data.company_name); 
                $('#company_website').html(website_company); 
                $('#company_email').html(action_data.company_email); 
                $('#modal_company').modal('show');
            });
            $('#table_data').on('click', '.btn_edit', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'get',
                                url            : site + '/employee/' + action_data.id + '/edit',
                                async          : true,
                                dataType       : 'json',
                                success        : function(data){
                                    $('#form_data')[0].reset();
                                    $('#first_name').val(data.first_name);
                                    $('#last_name').val(data.last_name);
                                    $('#email').val(data.email);
                                    $('#phone').val(data.phone);
                                    $('#modal_form').find('.modal-title').text('Edit Data');
                                    $('#btn_process').text('Update');
                                    $('#form_data').attr('method', 'post');
                                    $('#_method').val('put');
                                    $('#form_data').attr('action', site + '/employee/' + data.id);
                                    var company_id = '';
                                    if(data.id_company != '0'){
                                        company_id = data.id_company;
                                    }
                                    s_company(company_id);
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
            });
            $('#table_data').on('click', '.btn_delete', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    html                : '<pre>Delete this data ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'No',
                    confirmButtonText   : 'Yes'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : action_data.id},
                                        url         : site + '/employee/delete',
                                        dataType    : "json",
                                        async       : true,
                                        success   : function(response){
                                            if(response.success){
                                                swal({
                                                    html                : '<pre>Data deleted successfully</pre>',
                                                    type                : "success",
                                                    background          : 'transparent',
                                                    allowOutsideClick   : false,
                                                    allowEscapeKey      : false, 
                                                    showConfirmButton   : false,
                                                    timer               : 1000
                                                }).then(function(){
                                                    setTimeout(function(){
                                                        table_data.ajax.reload();
                                                    },500);
                                                });
                                            }
                                        },
                                        error          : function(){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Connection lost' + '<br>' + 
                                                              'Please try again</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
        }
        function data_process(){
            $('#btn_process').on('click', function(event){
                event.preventDefault();
                event.stopImmediatePropagation();
                var ajax_method     = $('#form_data').attr('method');
                var ajax_url        = $('#form_data').attr('action');
                var form_data       = $('#form_data')[0];
                form_data           = new FormData(form_data);
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : ajax_method,
                                url            : ajax_url,
                                data           : form_data,
                                async          : true,
                                processData    : false,
                                contentType    : false,
                                cache          : false,
                                dataType       : 'json',
                                success        : function(response){
                                    if(response.error){
                                        var errormessage    = '';
                                        for(i = 0; i < response.message.length; i ++){
                                            errormessage += response.message[i] + '\n';
                                        }
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>' + errormessage + '</pre>'
                                        });
                                    }
                                    if(response.success){
                                        $('#modal_form').modal('hide');
                                        $('#form_data')[0].reset();
                                        swal({
                                            html                : '<pre>Data ' + response.type + ' successfully</pre>',
                                            type                : "success",
                                            background          : 'transparent',
                                            allowOutsideClick   : false,
                                            allowEscapeKey      : false, 
                                            showConfirmButton   : false,
                                            timer               : 1000
                                        }).then(function(){
                                            setTimeout(function(){
                                                table_data.ajax.reload();
                                            },500);
                                        });
                                    }
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Connection lost' + '<br>' + 
                                                      'Please try again</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
                return false;
            });
        }
        function data_table(){
            table_data = $('#table_data').DataTable({
                lengthMenu          : [ [5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"] ],
                processing          : true, 
                destroy             : true,
                serverSide          : true, 
                scrollX             : true,
                scrollCollapse      : true,
                fixedColumns        : true, 
                initComplete: function(){
                    swal.close();
                    main();
                },
                ajax            : {
                    url         : site + '/employee',
                    method      : 'get'
                },
                columns             : [
                    {data   : 'DT_RowIndex'},
                    {data   : 'first_name',
                        render         : (data, type, row) => {
                            return row.first_name + ' ' + row.last_name;
                        }
                    },
                    {data   : 'company_name',
                        render         : (data, type, row) => {
                            return '<div style="text-align: center">' + row.company_name + '<br><button class="btn btn-sm btn-outline-primary btn_company_detail" id="' + row.company_id + '">Detail</button></div>';
                        }
                    },
                    {data   : 'email'},
                    {data   : 'phone'},
                    {data   : 'action'},
                    {data   : 'created_at',
                        render: function(created_at){
                            var time = created_at.split(' ');
                            return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                        }
                    },
                    {data   : 'updated_at',
                        render: function(updated_at){
                            if(updated_at != null){
                                var time = updated_at.split(' ');
                                return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                            }else{
                                return '';
                            }
                        }
                    }
                ]
            });
        }
    })
</script>
@endsection