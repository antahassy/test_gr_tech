<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \App\Models\M_company;
use \App\Models\M_employee;
use Validator;

class C_employee extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()){
            $data = M_employee::select(
                'id',
                'id_company',
                'first_name',
                'last_name',
                'email',
                'phone',
                'created_by',
                'created_at',
                'updated_by',
                'updated_at'
            )
            ->where('deleted_at', null)
            ->orderBy('id', 'asc')
            ->get();
            foreach ($data as $row) {
                $company = DB::table('tb_company')
                ->select(
                    'tb_company.name', 
                    'tb_company.email', 
                    'tb_company.logo', 
                    'tb_company.website'
                )
                ->where('tb_company.deleted_at', null)
                ->where('tb_company.id', $row->id_company)
                ->first();
                if($company){
                    $row->company_name = $company->name;
                    $row->company_email = $company->email;
                    $row->company_logo = $company->logo;
                    $row->company_website = $company->website;
                }else{
                    $row->company_name = '';
                    $row->company_email = '';
                    $row->company_logo = '';
                    $row->company_website = '';
                }
                $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>' . '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Delete</button>';
            }
            if($request->ajax()){
                return datatables()->of($data)->addIndexColumn()->toJson();
            }
            return view('employee.index')->with('active_menu', 'Employee');
        }else{
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
        ]);
        if(! $validation->passes()){
            $message = array_values($validation->errors()->toArray());
            $arr_messsage = array();
            foreach($message as $msg){
                array_push($arr_messsage, $msg[0]);
            }
            return response()->json([
                'error'     => true,
                'message'   => $arr_messsage
            ]);
        }else{
            $sess_id = Auth::id();
            $data = M_employee::create(
                [
                    'id_company'    => $request->company,
                    'first_name'    => $request->first_name,
                    'last_name'     => $request->last_name,
                    'email'         => $request->email,
                    'phone'         => $request->phone,
                    'created_by'    => $sess_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => null
                ]
            );
            if($data){
                return response()->json([
                    'success'   => true,
                    'type'      => 'saved'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_employee::find($id);
        return response()->json([
            'id'            => $data->id,
            'id_company'    => $data->id_company,
            'first_name'    => $data->first_name,
            'last_name'     => $data->last_name,
            'email'         => $data->email,
            'phone'         => $data->phone
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
        ]);
        if(! $validation->passes()){
            $message = array_values($validation->errors()->toArray());
            $arr_messsage = array();
            foreach($message as $msg){
                array_push($arr_messsage, $msg[0]);
            }
            return response()->json([
                'error'     => true,
                'message'   => $arr_messsage
            ]);
        }else{
            $sess_id = Auth::id();
            $data = M_employee::find($id);
            $data->id_company   = $request->company;
            $data->first_name   = $request->first_name;
            $data->last_name    = $request->last_name;
            $data->email        = $request->email;
            $data->phone        = $request->phone;
            $data->updated_by   = $sess_id;
            $data->updated_at   = date('Y-m-d H:i:s');
            $data->save();
            return response()->json([
                'success'   => true,
                'type'      => 'updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $sess_id = Auth::id();

        $id = $request->id;
        $data = M_employee::find($id);

        $data->deleted_by   = $sess_id;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true
        ]);
    }

    public function s_company(Request $request)
    {
        $data = M_company::select(
            'id',
            'name'
        )
        ->orderBy('id', 'asc')
        ->get();
        if($request->ajax()){
            return response()->json($data);
        }
    }
}
