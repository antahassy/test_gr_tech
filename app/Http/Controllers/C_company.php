<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use \App\Models\M_company;
use Validator;

class C_company extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()){
            $data = M_company::select(
                'id',
                'name',
                'email',
                'logo',
                'website',
                'created_by',
                'created_at',
                'updated_by',
                'updated_at'
            )
            ->where('deleted_at', null)
            ->orderBy('id', 'asc')
            ->get();
            foreach ($data as $row) {
                $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>' . '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Delete</button>';
            }
            if($request->ajax()){
                return datatables()->of($data)->addIndexColumn()->toJson();
            }
            return view('company.index')->with('active_menu', 'Company');
        }else{
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'company_name' => 'required|unique:tb_company,name',
        ]);
        if(! $validation->passes()){
            $message = array_values($validation->errors()->toArray());
            $arr_messsage = array();
            foreach($message as $msg){
                array_push($arr_messsage, $msg[0]);
            }
            return response()->json([
                'error'     => true,
                'message'   => $arr_messsage
            ]);
        }else{
            $sess_id = Auth::id();
            $delete_file = $request->delete_file;
            $last_file = $request->get_file;
            $file_path = public_path();
            $file_extension = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'ico');
            $file_size = 5120000;//5 mb
            $file_upload = $request->file('file');
            if($file_upload != ''){
                if($file_upload->getSize() > $file_size){
                    return response()->json([
                        'size'   => true
                    ]);
                }else{
                    if (in_array($file_upload->guessClientExtension(), $file_extension)){
                        $file = time() . '-' . $file_upload->getClientOriginalName();
                        $request->file->move($file_path, $file);
                        if($last_file != ''){
                            File::delete($file_path . '/' . $last_file);
                        }elseif($delete_file != ''){
                            File::delete($file_path . '/' . $delete_file);
                        }
                    }else{
                        return response()->json([
                            'extension'   => true
                        ]);
                    }
                }
            }else{
                if($last_file == '' && $delete_file != ''){
                    File::delete($file_path . '/' . $delete_file);
                    $file  = '';
                }elseif($last_file == '' && $delete_file == ''){
                    $file  = '';
                }else{
                    $file  = $last_file;
                }
            }
            $data = M_company::create(
                [
                    'name'          => $request->company_name,
                    'email'         => $request->company_email,
                    'logo'          => $file,
                    'website'       => $request->company_website,
                    'created_by'    => $sess_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => null
                ]
            );
            if($data){
                return response()->json([
                    'success'   => true,
                    'type'      => 'saved'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_company::find($id);
        return response()->json([
            'id'            => $data->id,
            'name'          => $data->name,
            'email'         => $data->email,
            'logo'          => $data->logo,
            'website'       => $data->website
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'company_name' => 'required|unique:tb_company,name,' . $id,
        ]);
        if(! $validation->passes()){
            $message = array_values($validation->errors()->toArray());
            $arr_messsage = array();
            foreach($message as $msg){
                array_push($arr_messsage, $msg[0]);
            }
            return response()->json([
                'error'     => true,
                'message'   => $arr_messsage
            ]);
        }else{
            $sess_id = Auth::id();
            $delete_file = $request->delete_file;
            $last_file = $request->get_file;
            $file_path = public_path();
            $file_extension = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'ico');
            $file_size = 5120000;//5 mb
            $file_upload = $request->file('file');
            if($file_upload != ''){
                if($file_upload->getSize() > $file_size){
                    return response()->json([
                        'size'   => true
                    ]);
                }else{
                    if (in_array($file_upload->guessClientExtension(), $file_extension)){
                        $file = time() . '-' . $file_upload->getClientOriginalName();
                        $request->file->move($file_path, $file);
                        if($last_file != ''){
                            File::delete($file_path . '/' . $last_file);
                        }elseif($delete_file != ''){
                            File::delete($file_path . '/' . $delete_file);
                        }
                    }else{
                        return response()->json([
                            'extension'   => true
                        ]);
                    }
                }
            }else{
                if($last_file == '' && $delete_file != ''){
                    File::delete($file_path . '/' . $delete_file);
                    $file  = '';
                }elseif($last_file == '' && $delete_file == ''){
                    $file  = '';
                }else{
                    $file  = $last_file;
                }
            }
            $data = M_company::find($id);
            $data->name         = $request->company_name;
            $data->email        = $request->company_email;
            $data->logo         = $file;
            $data->website      = $request->company_website;
            $data->updated_by   = $sess_id;
            $data->updated_at   = date('Y-m-d H:i:s');
            $data->save();
            return response()->json([
                'success'   => true,
                'type'      => 'updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $sess_id = Auth::id();

        $id = $request->id;
        $data = M_company::find($id);

        if($data->logo != null){
            $file_path = public_path();
            File::delete($file_path . '/' . $data->logo);
        }

        $data->logo         = null;
        $data->deleted_by   = $sess_id;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true
        ]);
    }
}