<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\C_company;
use App\Http\Controllers\C_employee;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Route::get('/', [HomeController::class, 'index']);

// Route::resource('company', C_company::class);
// Route::post('company/delete', [C_company::class, 'delete']);

// Route::resource('employee', C_employee::class);
// Route::post('employee/delete', [C_employee::class, 'delete']);
// Route::post('employee/s_company', [C_employee::class, 's_company']);

Route::middleware(['auth'])->group(function () {
 
 	Route::get('/', [HomeController::class, 'index']);
 
    Route::middleware(['admin_status'])->group(function () {
        Route::resource('company', C_company::class);
		Route::post('company/delete', [C_company::class, 'delete']);

		Route::resource('employee', C_employee::class);
		Route::post('employee/delete', [C_employee::class, 'delete']);
		Route::post('employee/s_company', [C_employee::class, 's_company']);
    });
 
});